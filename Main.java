import java.util.*;

public class Main {

    public static void main(String[] args) {

        int [] numbers = {14, 54, 25, 95, 14, 54, 74, 65, 10, 14, 56, 32, 85, 10, 23, 65, 59, 14};

        Scanner scan = new Scanner(System.in);

        System.out.print("Please enter a number from the array list: " + Arrays.toString(numbers) + " ----> ");

        int num = scan.nextInt();

        for (int i = 0; i < numbers.length; i++) {
            if (num == numbers[i]) {
                System.out.println(i);
            }

        }

    }

}
